A simple (and messy) script which periodically checks MemoryExpress.com to see if items are in stock. Displays a windows notification and sends an email.

Used to snag a gtx 1070 when it first came out.