var request = require('request');
var cheerio = require('cheerio');
var notifier = require('node-notifier');
var open = require('open');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var url = 'http://www.memoryexpress.com/Search/Products?Search=gtx+1070';

PerformCheck();

setInterval(function() {
    console.log('Running check...');
	PerformCheck();
}, 900000);

function PerformCheck() {
	request(url, function(error, response, html){
		if(!error){
			var $ = cheerio.load(html);
			var results = [];
			var products = $(".Product-IconView");
			
			console.log('Found ' + products.length + ' products.');		
			
			products.filter(function() {
				var data = $(this);
				var json = { id: "", name: "", cost: "", inStock: false };
				json.id = data.find('.ProductId').text();
				json.name = data.find('.ProductTitle').text();
				json.cost = data.find('.PIV_Price span').text();
				json.inStock = data.find('.InventoryStatus').length == 0;
				
				results.push(json);
			});
			
			//console.log(results);
			AnalyzeStock(results);
		}
		else
			Alert('There was an error: ' + error);
	});
}

function AnalyzeStock(results) {
	console.log('Analyzing stock...');

	if(results.length > 5)
		Alert('New product available.');
	else {
		for(var i = 0; i < results.length; i++) {
			if(results[i].name.toLowerCase().indexOf('founders') == -1) {
				if(results[i].inStock)
					Alert('Found stock for ' + results[i].name + '! Cost: ' + results[i].cost);
				else
					console.log('Found card of interest, but out of stock: ' + results[i].name);
			}
			else
				console.log('Ignoring founders edition model: ' + results[i].name);
		}		
	}
}

function Alert(message) {
	console.log(message);
	notifier.notify({
	  title: 'GTX 1070 Stock Checker',
	  message: message,
	  sound: true,
	  wait: true 
	});
	
	notifier.on('click', function (notifierObject, options) {
		open(url);
	});

	var transport = nodemailer.createTransport(smtpTransport({
		service: 'gmail',
		auth: {
			user: '***@gmail.com', // my mail
			pass: '***'
		}
	}));
	
	var mailOptions = {
		from: '***@gmail.com', 
		to: '***@gmail.com', 
		subject: 'GTX 1070 Stock Checker', 
		text: message
	};
	
	transport.sendMail(mailOptions, function(error, info){
		if(error){
			console.log(error);
		}else{
			console.log('Message sent: ' + info.response);
		};
	});
}
